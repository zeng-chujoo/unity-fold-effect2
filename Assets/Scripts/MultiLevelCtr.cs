using System;
using System.Collections;
using System.Collections.Generic;
using SoulGames.EasyGridBuilderPro;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MultiLevelCtr : MonoBehaviour
{
    // private MultiGridUIManager _multiGridUIManager;

    private Transform topPanel;
    private Transform leftPanel;
    private Transform rightPanel;
    private Transform bigCategoryContainer;
    private Transform contentForBigCategoryPanel;

    private Animator topPanelAnimator;
    private Animator leftPanelAnimator;
    private Animator rightPanelAnimator;
    
    private static readonly int Open = Animator.StringToHash("Open");
    private static readonly int Close = Animator.StringToHash("Close");

    private void Awake()
    {
        // _multiGridUIManager = GetComponent<MultiGridUIManager>();
        topPanel = transform.Find("UI Panel/TopPanel");
        leftPanel = transform.Find("UI Panel/LeftPanel");
        rightPanel = transform.Find("UI Panel/RightPanel");
        bigCategoryContainer = leftPanel.Find("LeftLeftPanel/Scroll View/Viewport/Content");
        contentForBigCategoryPanel = leftPanel.Find("LeftRightPanel/Scroll View/Viewport/Content");

        topPanelAnimator = topPanel.GetComponent<Animator>();
        leftPanelAnimator = leftPanel.GetComponent<Animator>();
        rightPanelAnimator = rightPanel.GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        //面板动画控制
        if (Input.GetKeyDown(KeyCode.Q))
        {
            leftPanelAnimator.SetTrigger(Open);
            rightPanelAnimator.SetTrigger(Open);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            leftPanelAnimator.SetTrigger(Close);
            rightPanelAnimator.SetTrigger(Close);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            int tmp = Random.Range(0, 999);
            CreateBigCategoryAndPanel(tmp+"类");
        }
    }

    /// <summary>
    /// 创建大类按钮。TODO:添加监听，设置打开关闭对应的大类容器面板
    /// </summary>
    /// <param name="bigCategoryName"></param>
    public void CreateBigCategoryAndPanel(string bigCategoryName)
    {
        GameObject BigCategory = Instantiate(Resources.Load<GameObject>("Prefab/UI/UICategory/大类按钮"),bigCategoryContainer);
        BigCategory.name = bigCategoryName;
        BigCategory.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = bigCategoryName;
        
        CreateBigCateforyPanelContainer(bigCategoryName);
        BigCategory.GetComponent<Button>().onClick.AddListener(delegate { OpenBigCategoryPanelAndCloseOther(bigCategoryName);});
    }

    /// <summary>
    /// 创建一个大类对应的面板容器
    /// </summary>
    /// <param name="bigCategoryName"></param>
    public void CreateBigCateforyPanelContainer(string bigCategoryName)
    {
        GameObject bigCategoryPanelContainer = Instantiate(Resources.Load<GameObject>("Prefab/UI/UICategory/一个大类对应的面板容器"),contentForBigCategoryPanel);
        bigCategoryPanelContainer.name = bigCategoryName;
        
        //为该面板创建几个小类的折叠容器
        int tmp = Random.Range(0, 10);
        for (int i = 0; i < tmp; i++)
        {
            GameObject smallCategory = Instantiate(Resources.Load<GameObject>("Prefab/UI/UICategory/一个小类的折叠容器"),bigCategoryPanelContainer.transform);
            smallCategory.name =  "小类" + i;
            smallCategory.transform.Find("容器标题按钮/图片文字/Title").GetComponent<TextMeshProUGUI>().text = "小类" + i;
            
            //为改小类面板创建几个物体按钮
            int count = Random.Range(0, 30);
            for (int j = 0; j < count; j++)
            {
                GameObject objBtn = Instantiate(Resources.Load<GameObject>("Prefab/UI/UICategory/物体按钮"),smallCategory.transform.Find("格子按钮容器"));
                objBtn.name =  "物体" + j;
                objBtn.transform.Find("Title").GetComponent<TextMeshProUGUI>().text = "物体" + j;
            }

        }
    }

    public void OpenBigCategoryPanelAndCloseOther(string name)
    {
        int childCount = contentForBigCategoryPanel.childCount;
        for (int i = 0; i < childCount; i++)
        {
            if (contentForBigCategoryPanel.GetChild(i).name!=name)
            {
                contentForBigCategoryPanel.GetChild(i).gameObject.SetActive(false);
            }
            else
            {
                contentForBigCategoryPanel.GetChild(i).gameObject.SetActive(true);
            }
        }
    }
}
