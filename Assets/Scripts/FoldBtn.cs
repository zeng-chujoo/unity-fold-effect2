using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FoldBtn : MonoBehaviour
{
    //父物体遮罩
    private RectTransform maskRect;
    //遮罩下面的标题空间（现在是按钮）
    private RectTransform titlePanel;
    //格子容器
    private RectTransform objBtnsContaner;
    //箭头图片
    private RectTransform imgTransform;
    //折叠按钮
    private Button foldBtn;
    private GridLayoutGroup _gridLayoutGroup;
    //改变速度
    private float changeSpeed = 1.25f;
    private bool isOpen;
    // Start is called before the first frame update
    void Awake()
    {
        maskRect = transform.parent.GetComponent<RectTransform>();
        titlePanel = GetComponent<RectTransform>();
        foldBtn = GetComponent<Button>();
        
        objBtnsContaner = transform.parent.Find("格子按钮容器").GetComponent<RectTransform>();
        _gridLayoutGroup = objBtnsContaner.GetComponent<GridLayoutGroup>();
        
        imgTransform = transform.Find("图片文字/Image").GetComponent<RectTransform>();
        foldBtn.onClick.AddListener(OnFoldBtnClick);
    }

    // private void Start()
    // {
    //     SetFoldOpenOrClose(false);
    // }

    public void SetFoldOpenOrClose(bool flag)
     {
        if(isOpen==flag) return;

        isOpen = flag;
        SetFoldState(isOpen);
        SetArrowDir();
    }

    private void OnFoldBtnClick()
    {
        isOpen = !isOpen;
        SetFoldState(isOpen);
        SetArrowDir();
    }
    private void SetFoldState(bool foldState)
    {
        if (objBtnsContaner == null) objBtnsContaner = transform.parent.Find("格子按钮容器").GetComponent<RectTransform>();
        if (objBtnsContaner.childCount!=0)
        {
            //StartCoroutine(ChangeRectHeight());
            DoChangeRectHeight();
        }
        //objBtnsContaner.gameObject.SetActive(foldState);
    }

    private void SetArrowDir()
    {
        if (isOpen)
        {
            imgTransform.DORotate(new Vector3(0, 0, 0), 0.2f);
        }
        else
        {
            imgTransform.DORotate(new Vector3(0, 0, 90), 0.2f);
        }
    }
    
    IEnumerator ChangeRectHeight()
    {
        //blockRect锚点的y(锚点在顶部，则y表示上边距离父物体顶部)
        float maxHeight = Math.Abs(objBtnsContaner.anchoredPosition.y)
                          + (Math.Abs(objBtnsContaner.GetChild(objBtnsContaner.childCount - 1).GetComponent<RectTransform>().anchoredPosition.y)+_gridLayoutGroup.cellSize.y/2 + _gridLayoutGroup.padding.bottom);
        
        
        float targetVal = isOpen ? titlePanel.rect.height : maxHeight;

        float t = 0;
        while (t<changeSpeed)
        {
            yield return null;
            float value = Mathf.Lerp(maskRect.sizeDelta.y, targetVal, t / changeSpeed);
            t += Time.deltaTime;
            //高度改变
            maskRect.sizeDelta = new Vector2(titlePanel.sizeDelta.x, value);
            maskRect.DOSizeDelta(new Vector2(titlePanel.sizeDelta.x, value), 0.5f);
        }
    }

    private void DoChangeRectHeight()
    {
        //blockRect锚点的y(锚点在顶部，则y表示上边距离父物体顶部)
        float maxHeight = Math.Abs(objBtnsContaner.anchoredPosition.y)
                          + (Math.Abs(objBtnsContaner.GetChild(objBtnsContaner.childCount - 1).GetComponent<RectTransform>().anchoredPosition.y)+_gridLayoutGroup.cellSize.y/2 + _gridLayoutGroup.padding.bottom);
        
        
        float targetVal = isOpen ? maxHeight : titlePanel.rect.height;

        maskRect.DOSizeDelta(new Vector2(titlePanel.sizeDelta.x, targetVal), 0.2f);
    }
}
