using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class SystemTip : MonoBehaviour
{
    public static SystemTip instance;
    private GameObject tipsRoot;        //Tip的根节点
    private const int CountTime = 5;    //倒计时销毁时间
    private float destroyTime;          //销毁计时
    private GameObject lastTipObj;      //上一个Tip对象
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
        tipsRoot = transform.Find("TipsRoot").gameObject;
        ClearList();
    }
    

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            ShowSystemTip(Random.Range(0,100).ToString());
        }
        destroyTime += Time.deltaTime;
        if ((int)destroyTime == CountTime)
        {
            if (lastTipObj)
            {
                DestroyImmediate(lastTipObj);
            }
        }
    }

    public void ShowSystemTip(string text)
    {
        //如果上一个Tip不为空，则执行动画后删除
        if (lastTipObj != null)
        {
            CanvasGroup canvasGroup = lastTipObj.GetComponent<CanvasGroup>();
            lastTipObj.transform.DOLocalMoveY(40, 0.5f);
            DOTween.To(() => canvasGroup.alpha, x => canvasGroup.alpha = x, 0, 0.5f)
                .OnComplete(() => DestroyImmediate(canvasGroup.gameObject));
        }
        //创建新的Tip
        GameObject tip = (GameObject)Instantiate(Resources.Load("Prefabs/UI/Common/Items/SystemTipItem"), tipsRoot.transform, false);
        destroyTime = 0;
        tip.transform.GetComponentInChildren<Text>().text = text;
        //执行动画
        tip.transform.DOLocalMoveY(0, 0.5f);
        CanvasGroup tipCanvasGroup = tip.GetComponent<CanvasGroup>();
        DOTween.To(() => tipCanvasGroup.alpha, x => tipCanvasGroup.alpha = x, 1, 0.5f);
        //设置LastTip
        lastTipObj = tip;
    }
    /// <summary>
    /// 清空列表
    /// </summary>
    private void ClearList()
    {
        //每删除一个，transform.childCount会减1，不能直接用这个变量去循环
        int listCount = tipsRoot.transform.childCount;
        for (int i = 0; i < listCount; i++)
        {
            GameObject item = tipsRoot.transform.GetChild(0).gameObject;
            DestroyImmediate(item);
        }
    }
}
